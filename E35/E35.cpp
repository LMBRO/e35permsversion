// E35.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using std::vector;
using std::string;
using std::stoi;
using std::to_string;
using std::cout;
using std::endl;
std::vector<unsigned int> const FACT_VALS{ 1,1,2,6,24,120,720 };
vector<bool> getSieve() {
	vector<bool> sieve(1000000, true);
	sieve[0] = sieve[1] = false; // 0 and 1 are not prime
	unsigned int i = 2;
	while (i <= 1000) {
		if (sieve[i]) {
			for (unsigned int j = i * 2; j < sieve.size(); j += i) {
				sieve[j] = false;
			}
		}
		++i;
	}
	return sieve;
}
bool getPerms(string ns, unsigned int p, vector<string> & perms, vector<bool> const & sieve) {
	if (p == ns.size() - 1) {
		if (sieve[stoi(ns)]) {
			perms.push_back(ns);
			return true;
		}
		else
			return false;
	}
	unsigned int pos = p;
	string copy = ns;
	for (unsigned int pos = p; pos != ns.size(); ++pos) {
		std::swap(copy[p], copy[pos]);
		bool ret = getPerms(copy, ++p, perms, sieve);
		if (!ret)
			return false;
		copy = ns;
	}
	return true;
}

bool getPerms(unsigned int n, vector<bool> & sieve, vector<bool>  &res, unsigned int & count) {
	if (n < 10) {
		if (sieve[n]) {
			res[n] = true;
			cout << n << endl;
			++count;
			return true;
		}
		return false;
	}
	if (res[n]) //redundant
		return true;
	vector<string> ress;
	string s = to_string(n);
	string copy = s;
	for (unsigned int pos = 0; pos != s.size(); ++pos) {
		std::swap(copy[0], copy[pos]);
		bool ret = getPerms(copy, 1, ress, sieve);
		if (!ret)
			return false;
		copy = s;
	}
	for (string const & s : ress) {
		if (!res[stoi(s)]) {
			res[stoi(s)] = true;
			cout << s << endl;
			++count;
		}
	}
	return true;
}
int main()
{
	unsigned int pcircular_count = 0;
	vector<bool> ispCircular(1000000);
	vector<bool> sieve = getSieve();
	for (unsigned int p = 2; p != sieve.size(); ++p) {
		if (sieve[p] && !ispCircular[p]) {
			getPerms(p, sieve, ispCircular, pcircular_count);
		}
	}
		cout << "n: " << pcircular_count << endl;
    return 0;
}

